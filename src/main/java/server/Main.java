package server;

import server.service.auth.AuthService;
import server.service.chat.Chat;
import server.service.matchmaker.Matchmaker;
import server.service.server.Server;

public class Main {
    public static void main(String[] args) {
        Server server = new Server(3000);
        server.addService(new Chat());
        server.addService(new AuthService());
        server.addService(new Matchmaker());

        server.listen();
    }
}
