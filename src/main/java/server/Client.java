package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import server.service.ServiceClient;

public class Client extends Thread {
    private PrintWriter output;
    private BufferedReader input;

    private AtomicBoolean listenning = new AtomicBoolean(true);

    private CopyOnWriteArrayList<ServiceClient> observers = new CopyOnWriteArrayList<>();

    public Client(Socket socket) {
        try {
            output = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listen() {
        try {
            while (listenning.get()) {
                String message = input.readLine();
                sendAll(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            output.close();
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void send(String message) {
        output.println(message);
    }

    public void disconnectFromServices() {
        for (ServiceClient serviceClient : observers) {
            serviceClient.disconnectFromService();
        }
        close();
    }

    public void close() {
        listenning.set(false);
    }

    @Override
    public void run() {
        listen();
    }

    public void addObserver(ServiceClient serviceClient) {
        observers.add(serviceClient);
    }

    public void removeObserver(ServiceClient serviceClient) {
        observers.remove(serviceClient);
    }

    public void sendAll(String message) {
        for (ServiceClient observer : observers) {
            observer.sendToService(message);
        }
    }
}