package server.service;

import server.Client;

public abstract class ServiceClient {
    protected Service service;
    protected Client client;

    public ServiceClient(Service service, Client client) {
        this.service = service;
        this.client = client;
    }

    public void link() {
        service.addClient(this);
        client.addObserver(this);
    }

    public void sendToService(String message) {
        service.send(this, message);
    }

    public void sendToClient(String message) {
        client.send(message);
    }

    public void disconnectFromService() {
        service.removeClient(this);
        client.removeObserver(this);
    }

    public Client getClient() {
        return client;
    }
}