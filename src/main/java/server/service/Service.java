package server.service;

import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;

import server.service.server.Server;

public abstract class Service<T extends ServiceClient> {
    protected CopyOnWriteArrayList<T> clients = new CopyOnWriteArrayList<>();
    protected Server server;
    private final String name;

    public Service(String name) {
        this.name = name;
    }

    public void addClient(T client) {
        clients.add(client);
        clientAdded(client);
    }

    public abstract void clientAdded(T client);

    public void removeClient(T client) {
        clients.remove(client);
        clientremoved(client);
    }

    public abstract void clientremoved(T client);

    public String getName() {
        return name;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public void shutdown() {
        for (T client : clients) {
            client.disconnectFromService();
        }
    }

    protected String removeServiceName(String[] string) {
        String[] sub = Arrays.copyOfRange(string, 1, string.length);
        return String.join(" ", sub);
    }

    public void send(T client, String message) {
        String[] splittedMessage = message.split(" ");
        if (splittedMessage[0].equals(name)) {
            switch (splittedMessage[1]) {
            case "shutdown":
                shutdown();
                break;
            case "disconnect":
                client.disconnectFromService();
                break;
            default:
                receive(client, removeServiceName(splittedMessage));
                break;
            }
        }
    }

    public abstract void receive(T client, String message);
}