package server.service.chat;

import server.service.Service;

public class Chat extends Service<ChatClient> {

    public Chat() {
        super("chat");
    }

    private void broadcast(String message) {
        for (ChatClient chatClient : clients) {
            chatClient.sendToClient(message);
        }
    }

    @Override
    public void receive(ChatClient client, String message) {
        String[] splittedMessage = message.split(" ");
        switch (splittedMessage[0]) {
        case "setname":
            if (splittedMessage.length == 2) {
                client.setUsername(splittedMessage[1]);
            }
            break;
        }
        broadcast(client.getUsername() + " : " + message);
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void clientAdded(ChatClient client) {
        // broadcast("new client in chat");
    }

    @Override
    public void clientremoved(ChatClient client) {
        // broadcast(client.getUsername() + " left");
    }

}