package server.service.chat;

import server.Client;
import server.service.Service;
import server.service.ServiceClient;

public class ChatClient extends ServiceClient {
    private String userame = "user";

    public ChatClient(Service service, Client client) {
        super(service, client);
    }

    public String getUsername() {
        return userame;
    }

    public void setUsername(String usernam) {
        this.userame = usernam;
    }
}