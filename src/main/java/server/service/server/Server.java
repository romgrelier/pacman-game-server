package server.service.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;

import server.Client;
import server.service.Service;
import server.service.auth.AuthClient;
import server.service.chat.ChatClient;

public class Server extends Service<ServerClient> {
    private ServerSocket serverSocket;

    private CopyOnWriteArrayList<Service<?>> services = new CopyOnWriteArrayList<>();

    private boolean listenning = true;

    public Server(final int port) {
        super("hub");
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listen() {
        while (listenning) {
            try {
                Socket socket = serverSocket.accept();
                Client client = new Client(socket);
                client.start();

                ServerClient serverClient = new ServerClient(this, client);
                serverClient.link();
                ChatClient chatClient = new ChatClient(services.get(0), client);
                chatClient.link();
                AuthClient authClient = new AuthClient(services.get(1), client);
                authClient.link();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stop() {
        listenning = false;
    }

    public void addService(Service service) {
        service.setServer(this);
        services.add(service);
    }

    public void removeService(Service service) {
        services.remove(service);
    }

    public Service getService(int id) {
        return services.get(id);
    }

    @Override
    public void receive(ServerClient client, String message) {
        String[] splittedMessage = message.split(" ");
        switch (splittedMessage[0]) {
        case "sub":
            break;
        }
    }

    @Override
    public void shutdown() {
        for (Service service : services) {
            service.shutdown();
        }
        for (ServerClient client : clients) {
            client.disconnectFromService();
        }
        stop();
    }

    @Override
    public void clientAdded(ServerClient client) {

    }

    @Override
    public void clientremoved(ServerClient client) {

    }

}