package server.service.server;

import server.Client;
import server.service.ServiceClient;

public class ServerClient extends ServiceClient {
    public ServerClient(Server server, Client client) {
        super(server, client);
    }

    @Override
    public void disconnectFromService() {
        super.disconnectFromService();
        client.close();
    }
}