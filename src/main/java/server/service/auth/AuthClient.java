package server.service.auth;

import server.Client;
import server.service.Service;
import server.service.ServiceClient;

public class AuthClient extends ServiceClient {
    private String key = "";

    public AuthClient(Service service, Client client) {
        super(service, client);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}