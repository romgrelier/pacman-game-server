package server.service.auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import pacman.Logger;
import server.service.Service;
import server.service.matchmaker.User;
import server.service.pacman.beans.Player;
import server.service.pacman.dao.DAOFactory;
import server.service.pacman.dao.PlayerDAO;
import server.service.pacman.dao.DAOFactory.DataStorageType;

public class AuthService extends Service<AuthClient> {
    private DAOFactory daoFactory = DAOFactory.getDaoFactory(DataStorageType.MARIADB);
    private PlayerDAO playerDAO = daoFactory.getPlayerDAO();

    public AuthService() {
        super("auth");
    }

    private static String hashMD5(String string) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            md.update(string.getBytes());

            byte[] bytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; ++i) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "incorrect";
    }

    private boolean connect(AuthClient client, String username, String password) {
        Player playerToConnect = playerDAO.find(username);
        if (playerToConnect != null) {
            String hash = hashMD5(password);

            if (hash.equals(playerToConnect.getPassword())) {
                // client.sendToClient("connected");
                // send the key
                Random rand = new Random();
                String key = String.valueOf(rand.nextInt(1000000));
                client.setKey(key);
                client.sendToClient("key " + key);

                User user = new User(server.getService(2), client.getClient(), client.getKey(), playerToConnect);
                user.link();

            } else if (hash.equals("incorrect")) {
                client.sendToClient("key failed");
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    @Override
    public void receive(AuthClient client, String message) {
        String[] splittedMessage = message.split(" ");

        switch (splittedMessage[0]) {
        case "connect":
            if (splittedMessage.length == 3) {
                String username = splittedMessage[1];
                String password = splittedMessage[2];
                if (connect(client, username, password)) {
                    removeClient(client); // remove from the auth service
                }
            }
            break;
        }
    }

    @Override
    public void clientAdded(AuthClient client) {
        Logger.getInstance().send("AuthService", "new client");
    }

    @Override
    public void clientremoved(AuthClient client) {

    }

}