package server.service.instance;

import server.Client;
import server.service.Service;
import server.service.ServiceClient;
import server.service.pacman.beans.GamePlayedByPlayerAsPacman;

public class Player extends ServiceClient {
    private GamePlayedByPlayerAsPacman player;

    public Player(Service service, Client client, GamePlayedByPlayerAsPacman player) {
        super(service, client);
        this.player = new GamePlayedByPlayerAsPacman();
        this.player = player;
    }

    public GamePlayedByPlayerAsPacman getPlayer() {
        return player;
    }
}