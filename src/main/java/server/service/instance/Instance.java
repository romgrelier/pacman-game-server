package server.service.instance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pacman.Logger;
import pacman.command.Command;
import pacman.command.game.ResetCommand;
import pacman.controller.FrontController;
import pacman.game.PacmanGame;
import server.service.Service;
import server.service.matchmaker.Matchmaker;
import server.service.pacman.beans.Game;
import server.service.pacman.dao.DAOFactory;
import server.service.pacman.dao.GameDAO;
import server.service.pacman.dao.MapDAO;
import server.service.pacman.dao.DAOFactory.DataStorageType;
import pacman.mvc.*;
import pacman.observer.Observer;
import pacman.view.MazeGameView;

public class Instance extends Service<Player> implements Observer {
    private String map = "";
    private Matchmaker matchmaker;
    private PacmanGame pacmanGame = new PacmanGame(1000);
    FrontController controller;

    private DAOFactory daoFactory = DAOFactory.getDaoFactory(DataStorageType.MARIADB);
    private GameDAO gameDAO = daoFactory.getGameDAO();
    private MapDAO mapDAO = daoFactory.getMapDAO();
    private Game game = new Game();

    private MazeGameView gameView;

    public Instance(String name, Matchmaker matchmaker) {
        super(name);
        this.matchmaker = matchmaker;

        try {
            pacmanGame.changeMaze("src/main/resources/layout/contestClassic.lay");
        } catch (Exception e) {
            e.printStackTrace();
        }

        controller = new FrontController(FrontController.ControllerBehavior.SERVER);
        controller.attachReceiver(pacmanGame);

        pacmanGame.addObserver(this);

        // gameView = new MazeGameView();
        // gameView.attachModel(pacmanGame);

        pacmanGame.sendCommand(new ResetCommand());

        game.setMap(mapDAO.find(Long.valueOf(7)));
    }

    @Override
    public void receive(Player client, String message) {
        String[] splittedMessage = message.split(" ");
        switch (splittedMessage[0]) {
        case "CHGDIR":
            switch (splittedMessage[1]) {
            case "NORTH":
                controller.sendInput(Controller.Input.Z);
                break;
            case "SOUTH":
                controller.sendInput(Controller.Input.S);
                break;
            case "EAST":
                controller.sendInput(Controller.Input.D);
                break;
            case "WEST":
                controller.sendInput(Controller.Input.Q);
                break;
            }
            break;
        case "COM":
            switch (splittedMessage[1]) {
            case "RESET":
                controller.sendInput(Controller.Input.RESET);
                client.sendToClient(":pacman reset");
                break;
            case "RUN":
                controller.sendInput(Controller.Input.RUN);
                client.sendToClient(":pacman run");
                break;
            case "STEP":
                controller.sendInput(Controller.Input.STEP);
                client.sendToClient(":pacman step");
                break;
            case "PAUSE":
                controller.sendInput(Controller.Input.PAUSE);
                client.sendToClient(":pacman pause");
                break;
            case "QUIT":
                controller.sendInput(Controller.Input.QUIT);
                break;
            case "MAP":
                controller.sendInput(Controller.Input.CHGMAZE, splittedMessage[2]);
                break;
            }
            break;
        }

        if (pacmanGame.getPacmans().get(0).isDead()) {
            client.getPlayer().setScore(pacmanGame.getPacmans().get(0).getScore());
            Logger.getInstance().send("Instance", "" + client.getPlayer().getScore());
        }
    }

    private void broadcastState() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(pacmanGame.getMaze());
        broadcast(":pacman STATE" + ' ' + json);
    }

    private void broadcast(String message) {
        for (Player player : clients) {
            player.sendToClient(message);
        }
    }

    @Override
    public void clientAdded(Player client) {

    }

    @Override
    public void clientremoved(Player client) {

    }

    @Override
    public void update(Event event) {
        broadcastState();

        switch (event) {
        case END:
            // record player score
            for (Player player : clients) {
                player.getPlayer().setScore(pacmanGame.getPacmans().get(0).getScore());
                player.getPlayer().setGame(game);
                game.getPlayerPacman().add(player.getPlayer());
            }

            // send to the database
            gameDAO.create(game);

            broadcast(":pacman end");

            // remove players
            for (Player player : clients) {
                player.disconnectFromService();
            }

            matchmaker.stopInstance(getName());

            break;
        }
    }
}