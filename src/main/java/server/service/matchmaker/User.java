package server.service.matchmaker;

import server.Client;
import server.service.Service;
import server.service.ServiceClient;
import server.service.pacman.beans.Player;

public class User extends ServiceClient {
    private String key = "";
    private Player player;

    public User(Service service, Client client, String key, Player player) {
        super(service, client);
        this.key = key;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}