package server.service.matchmaker;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import pacman.Logger;
import server.service.Service;
import server.service.instance.Instance;
import server.service.instance.Player;
import server.service.pacman.beans.GamePlayedByPlayerAsPacman;

public class Matchmaker extends Service<User> {
    ConcurrentHashMap<String, Instance> instances = new ConcurrentHashMap<>();

    public Matchmaker() {
        super("matchmaker");
    }

    private String create() {
        // create a new pacman game instance
        Random rand = new Random();
        String key = String.valueOf(rand.nextInt(1000000));

        Instance instance = new Instance(key, this);

        server.addService(instance);
        instances.put(key, instance);

        return key;
    }

    public void stopInstance(String key) {
        server.removeService(instances.get(key));
        instances.get(key).shutdown();
        instances.remove(key);
    }

    private void join(String instanceKey, User user) {
        GamePlayedByPlayerAsPacman g = new GamePlayedByPlayerAsPacman();
        g.setPlayer(user.getPlayer());

        Player player = new Player(instances.get(instanceKey), user.getClient(), g);
        player.link();
    }

    @Override
    public void receive(User client, String message) {
        String[] splittedMessage = message.split(" ");
        switch (splittedMessage[0]) {
        case "create": // create
            if (splittedMessage.length == 1) {
                String key = create();
                instances.get(key);
                client.sendToClient("key " + key);
            }
            break;
        case "join": // join idInstance
            if (splittedMessage.length == 2) {
                join(splittedMessage[1], client);
            }
            break;
        }
    }

    @Override
    public void clientAdded(User client) {
        Logger.getInstance().send(getName(), client.getClient().getName());
    }

    @Override
    public void clientremoved(User client) {

    }

}