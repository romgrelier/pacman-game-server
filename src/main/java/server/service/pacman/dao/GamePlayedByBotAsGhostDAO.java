package server.service.pacman.dao;

import server.service.pacman.beans.Bot;
import server.service.pacman.beans.Game;
import server.service.pacman.beans.GamePlayedByBotAsGhost;

import java.util.ArrayList;

public interface GamePlayedByBotAsGhostDAO {
    ArrayList<GamePlayedByBotAsGhost> findAll();

    ArrayList<GamePlayedByBotAsGhost> findGame(Bot bot);

    ArrayList<GamePlayedByBotAsGhost> findBot(Game game);

    void create(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);

    void update(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);

    void delete(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);
}
