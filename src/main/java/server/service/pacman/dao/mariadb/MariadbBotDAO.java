package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Bot;
import server.service.pacman.dao.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbBotDAO implements server.service.pacman.dao.BotDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ID = "SELECT * FROM Bot WHERE id = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM Bot";
    private final String SQL_INSERT = "INSERT INTO Bot(name) VALUES(?)";
    private final String SQL_UPDATE = "UPDATE Bot SET name = ? WHERE id = ?";
    private final String SQL_DELETE = "DELETE FROM Bot WHERE id = ?";

    private Bot map(ResultSet resultSet) throws SQLException {
        Bot bot = new Bot();

        bot.setId(resultSet.getLong("id"));
        bot.setName(resultSet.getString("name"));

        return bot;
    }

    private Bot mapScore(ResultSet resultSet) throws SQLException {
        Bot bot = map(resultSet);
        bot.setScore(resultSet.getInt("score"));

        return bot;
    }

    public MariadbBotDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Bot find(Long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Bot bot = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ID, false, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                bot = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return bot;
    }

    @Override
    public ArrayList<Bot> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Bot> bots = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Bot bot = map(resultSet);
                bots.add(bot);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return bots;
    }

    @Override
    public void create(Bot bot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, true, bot.getName());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                bot.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Bot creation failed : no new id generated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

    }

    @Override
    public void update(Bot bot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false, bot.getName(), bot.getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Bot bot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false, bot.getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }
}
