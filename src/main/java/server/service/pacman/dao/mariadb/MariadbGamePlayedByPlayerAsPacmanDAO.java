package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Game;
import server.service.pacman.beans.GamePlayedByPlayerAsPacman;
import server.service.pacman.beans.Player;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.GamePlayedByPlayerAsPacmanDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbGamePlayedByPlayerAsPacmanDAO implements GamePlayedByPlayerAsPacmanDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ALL = "SELECT * FROM Joint_player_pacman";
    private final String SQL_SELECT_BOT = "SELECT * FROM Joint_player_pacman WHERE id_player = ?";
    private final String SQL_SELECT_GAME = "SELECT * FROM Joint_player_pacman WHERE id_game = ?";

    private final String SQL_INSERT = "INSERT INTO Joint_player_pacman(id_game, id_player, score) VALUES(?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE Joint_player_pacman SET id_game = ?, id_player = ?, score = ? WHERE id_game = ?";
    private final String SQL_DELETE = "DELETE FROM Joint_player_pacman WHERE id_game = ?";

    private GamePlayedByPlayerAsPacman map(ResultSet resultSet) throws SQLException {
        GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = new GamePlayedByPlayerAsPacman();

        gamePlayedByPlayerAsPacman.setPlayer(daoFactory.getPlayerDAO().find(resultSet.getLong("id_player")));
        gamePlayedByPlayerAsPacman.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByPlayerAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsPacman;
    }

    private GamePlayedByPlayerAsPacman map(ResultSet resultSet, Game game) throws SQLException {
        GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = new GamePlayedByPlayerAsPacman();

        gamePlayedByPlayerAsPacman.setPlayer(daoFactory.getPlayerDAO().find(resultSet.getLong("id_player")));
        gamePlayedByPlayerAsPacman.setGame(game);
        gamePlayedByPlayerAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsPacman;
    }

    private GamePlayedByPlayerAsPacman map(ResultSet resultSet, Player player) throws SQLException {
        GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = new GamePlayedByPlayerAsPacman();

        gamePlayedByPlayerAsPacman.setPlayer(player);
        gamePlayedByPlayerAsPacman.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByPlayerAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsPacman;
    }

    public MariadbGamePlayedByPlayerAsPacmanDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsPacman> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsPacman> gamePlayedByPlayerAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = map(resultSet);
                gamePlayedByPlayerAsPacmans.add(gamePlayedByPlayerAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsPacmans;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsPacman> findGame(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsPacman> gamePlayedByPlayerAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_BOT, false, player.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = map(resultSet, player);
                gamePlayedByPlayerAsPacmans.add(gamePlayedByPlayerAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsPacmans;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsPacman> findPlayer(Game game) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsPacman> gamePlayedByPlayerAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_GAME, false, game.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman = map(resultSet, game);
                gamePlayedByPlayerAsPacmans.add(gamePlayedByPlayerAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsPacmans;
    }

    @Override
    public void create(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, false,
                    gamePlayedByPlayerAsPacman.getGame().getId(), gamePlayedByPlayerAsPacman.getPlayer().getId(),
                    gamePlayedByPlayerAsPacman.getScore());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void update(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false,
                    gamePlayedByPlayerAsPacman.getGame().getId(), gamePlayedByPlayerAsPacman.getPlayer().getId(),
                    gamePlayedByPlayerAsPacman.getScore(), gamePlayedByPlayerAsPacman.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false,
                    gamePlayedByPlayerAsPacman.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

}
