package server.service.pacman.dao.mariadb;

import server.service.pacman.dao.*;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MariadbDAOFactory extends server.service.pacman.dao.DAOFactory {
    private static final String PROPERTIES_FILE = "dao.mariadb.properties";
    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";

    private String url;
    private String username;
    private String password;

    public static MariadbDAOFactory getInstance() {
        Properties properties = new Properties();
        String url = "";
        String driver = "";
        String username = "";
        String password = "";

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream file = classLoader.getResourceAsStream(PROPERTIES_FILE);

        if (file == null) {
            throw new DAOConfigurationException(PROPERTIES_FILE + " not found");
        }

        try {
            properties.load(file);
            url = properties.getProperty(PROPERTY_URL);
            driver = properties.getProperty(PROPERTY_DRIVER);
            username = properties.getProperty(PROPERTY_USERNAME);
            password = properties.getProperty(PROPERTY_PASSWORD);
        } catch (IOException e) {
            throw new DAOConfigurationException(PROPERTIES_FILE + " impossible to load : " + e.getMessage());
        }

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new DAOConfigurationException("Driver : " + driver + " not found");
        }

        return new MariadbDAOFactory(url, username, password);
    }

    public MariadbDAOFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    @Override
    public GameDAO getGameDAO() {
        return new MariadbGameDAO(this);
    }

    @Override
    public MapDAO getMapDAO() {
        return new MariadbMapDAO(this);
    }

    @Override
    public BotDAO getBotDAO() {
        return new MariadbBotDAO(this);
    }

    @Override
    public PlayerDAO getPlayerDAO() {
        return new MariadbPlayerDAO(this);
    }

    @Override
    public GamePlayedByBotAsGhostDAO getGamePlayedByBotAsGhostDAO() {
        return new MariadbGamePlayedByBotAsGhostDAO(this);
    }

    @Override
    public GamePlayedByBotAsPacmanDAO getGamePlayedByBotAsPacmanDAO() {
        return new MariadbGamePlayedByBotAsPacmanDAO(this);
    }

    @Override
    public GamePlayedByPlayerAsGhostDAO getGamePlayedByPlayerAsGhostDAO() {
        return new MariadbGamePlayedByPlayerAsGhostDAO(this);
    }

    @Override
    public GamePlayedByPlayerAsPacmanDAO getGamePlayedByPlayerAsPacmanDAO() {
        return new MariadbGamePlayedByPlayerAsPacmanDAO(this);
    }

}
