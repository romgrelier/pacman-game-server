package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.*;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.GameDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbGameDAO implements GameDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ID = "SELECT * FROM Game WHERE id = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM Game";
    private final String SQL_INSERT = "INSERT INTO Game(date, id_map) VALUES(?, ?)";
    private final String SQL_UPDATE = "UPDATE Game SET name = ? WHERE id = ?";
    private final String SQL_DELETE = "DELETE FROM Game WHERE id = ?";

    private Game map(ResultSet resultSet) throws SQLException {
        Game game = new Game();

        game.setId(resultSet.getLong("id"));
        game.setDate(resultSet.getDate("date"));
        game.setMap(daoFactory.getMapDAO().find(resultSet.getLong("id_map")));
        game.setBotGhost(daoFactory.getGamePlayedByBotAsGhostDAO().findBot(game));
        game.setBotPacman(daoFactory.getGamePlayedByBotAsPacmanDAO().findBot(game));
        game.setPlayerghost(daoFactory.getGamePlayedByPlayerAsGhostDAO().findPlayer(game));
        game.setPlayerPacman(daoFactory.getGamePlayedByPlayerAsPacmanDAO().findPlayer(game));

        return game;
    }

    public MariadbGameDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Game find(Long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Game game = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ID, false, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                game = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return game;
    }

    public ArrayList<Game> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Game> games = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Game game = map(resultSet);
                games.add(game);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return games;
    }

    @Override
    public void create(Game game) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, true, game.getDate(),
                    game.getMap().getId());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                game.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Bot creation failed : no new id generated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        for (GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost : game.getPlayerghost()) {
            daoFactory.getGamePlayedByPlayerAsGhostDAO().create(gamePlayedByPlayerAsGhost);
        }

        for (GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman : game.getPlayerPacman()) {
            daoFactory.getGamePlayedByPlayerAsPacmanDAO().create(gamePlayedByPlayerAsPacman);
        }

        for (GamePlayedByBotAsGhost gamePlayedByBotAsGhost : game.getBotGhost()) {
            daoFactory.getGamePlayedByBotAsGhostDAO().create(gamePlayedByBotAsGhost);
        }

        for (GamePlayedByBotAsPacman gamePlayedByBotAsPacman : game.getBotPacman()) {
            daoFactory.getGamePlayedByBotAsPacmanDAO().create(gamePlayedByBotAsPacman);
        }
    }

    @Override
    public void update(Game game) {

    }

    @Override
    public void delete(Game game) {

    }
}
