package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Player;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.PlayerDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbPlayerDAO implements PlayerDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ID = "SELECT * FROM Player WHERE id = ?";
    private final String SQL_SELECT_NAME = "SELECT * FROM Player WHERE username = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM Player";
    private final String SQL_INSERT = "INSERT INTO Player(username, password) VALUES(?, MD5(?))";
    private final String SQL_UPDATE = "UPDATE Player SET username = ?, password = ? WHERE id = ?";
    private final String SQL_DELETE = "DELETE FROM Player WHERE id = ?";

    private Player map(ResultSet resultSet) throws SQLException {
        Player player = new Player();

        player.setId(resultSet.getLong("id"));
        player.setUsername(resultSet.getString("username"));
        player.setPassword(resultSet.getString("password"));

        return player;
    }

    public MariadbPlayerDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Player find(Long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Player player = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ID, false, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                player = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return player;
    }

    @Override
    public Player find(String name) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Player player = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_NAME, false, name);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                player = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return player;
    }

    public ArrayList<Player> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Player> players = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Player player = map(resultSet);
                players.add(player);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return players;
    }

    @Override
    public void create(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, true, player.getUsername(),
                    player.getPassword());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Player creation failed : no row added");
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                player.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Player creation failed : no new id generated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

    }

    @Override
    public void update(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false, player.getUsername(),
                    player.getPassword());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false, player.getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }
}
