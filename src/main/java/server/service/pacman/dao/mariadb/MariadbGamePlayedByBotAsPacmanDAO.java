package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Bot;
import server.service.pacman.beans.Game;
import server.service.pacman.beans.GamePlayedByBotAsPacman;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.GamePlayedByBotAsPacmanDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbGamePlayedByBotAsPacmanDAO implements GamePlayedByBotAsPacmanDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ALL = "SELECT * FROM Joint_bot_pacman";
    private final String SQL_SELECT_BOT = "SELECT * FROM Joint_bot_pacman WHERE id_bot = ?";
    private final String SQL_SELECT_GAME = "SELECT * FROM Joint_bot_pacman WHERE id_game = ?";

    private final String SQL_INSERT = "INSERT INTO Joint_bot_pacman(id_game, id_bot, score) VALUES(?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE Joint_bot_pacman SET id_game = ?, id_bot = ?, score = ? WHERE id_game = ?";
    private final String SQL_DELETE = "DELETE FROM Joint_bot_pacman WHERE id_game = ?";

    private GamePlayedByBotAsPacman map(ResultSet resultSet) throws SQLException {
        GamePlayedByBotAsPacman gamePlayedByBotAsPacman = new GamePlayedByBotAsPacman();

        gamePlayedByBotAsPacman.setBot(daoFactory.getBotDAO().find(resultSet.getLong("id_bot")));
        gamePlayedByBotAsPacman.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByBotAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsPacman;
    }

    private GamePlayedByBotAsPacman map(ResultSet resultSet, Game game) throws SQLException {
        GamePlayedByBotAsPacman gamePlayedByBotAsPacman = new GamePlayedByBotAsPacman();

        gamePlayedByBotAsPacman.setBot(daoFactory.getBotDAO().find(resultSet.getLong("id_bot")));
        gamePlayedByBotAsPacman.setGame(game);
        gamePlayedByBotAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsPacman;
    }

    private GamePlayedByBotAsPacman map(ResultSet resultSet, Bot bot) throws SQLException {
        GamePlayedByBotAsPacman gamePlayedByBotAsPacman = new GamePlayedByBotAsPacman();

        gamePlayedByBotAsPacman.setBot(bot);
        gamePlayedByBotAsPacman.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByBotAsPacman.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsPacman;
    }

    public MariadbGamePlayedByBotAsPacmanDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public ArrayList<GamePlayedByBotAsPacman> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsPacman> gamePlayedByBotAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByBotAsPacman gamePlayedByBotAsPacman = map(resultSet);
                gamePlayedByBotAsPacmans.add(gamePlayedByBotAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByBotAsPacmans;
    }

    @Override
    public ArrayList<GamePlayedByBotAsPacman> findGame(Bot bot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsPacman> gamePlayedByBotAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_BOT, false, bot.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByBotAsPacman gamePlayedByBotAsPacman = map(resultSet, bot);
                gamePlayedByBotAsPacmans.add(gamePlayedByBotAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByBotAsPacmans;
    }

    @Override
    public ArrayList<GamePlayedByBotAsPacman> findBot(Game game) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsPacman> gamePlayedByBotAsPacmans = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_GAME, false, game.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByBotAsPacman gamePlayedByBotAsPacman = map(resultSet, game);
                gamePlayedByBotAsPacmans.add(gamePlayedByBotAsPacman);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByBotAsPacmans;
    }

    @Override
    public void create(GamePlayedByBotAsPacman gamePlayedByBotAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, false,
                    gamePlayedByBotAsPacman.getGame().getId(), gamePlayedByBotAsPacman.getBot().getId(),
                    gamePlayedByBotAsPacman.getScore());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void update(GamePlayedByBotAsPacman gamePlayedByBotAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false,
                    gamePlayedByBotAsPacman.getGame().getId(), gamePlayedByBotAsPacman.getBot().getId(),
                    gamePlayedByBotAsPacman.getScore(), gamePlayedByBotAsPacman.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(GamePlayedByBotAsPacman gamePlayedByBotAsPacman) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false,
                    gamePlayedByBotAsPacman.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

}
