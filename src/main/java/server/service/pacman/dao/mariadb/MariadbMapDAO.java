package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Map;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.MapDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbMapDAO implements MapDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ID = "SELECT * FROM Map WHERE id = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM Map";
    private final String SQL_INSERT = "INSERT INTO Map(name, pacman_count, ghost_count) VALUES(?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE Map SET name = ?, pacman_count = ?, ghost_count = ? WHERE id = ?";
    private final String SQL_DELETE = "DELETE FROM Map WHERE id = ?";

    private Map map(ResultSet resultSet) throws SQLException {
        Map map = new Map();

        map.setId(resultSet.getLong("id"));
        map.setName(resultSet.getString("name"));
        map.setPacman_nb(resultSet.getInt("pacman_count"));
        map.setGhost_nb(resultSet.getInt("ghost_count"));

        return map;
    }

    public MariadbMapDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Map find(Long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Map map = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ID, false, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                map = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return map;
    }

    public ArrayList<Map> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Map> maps = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Map map = map(resultSet);
                maps.add(map);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return maps;
    }

    @Override
    public void create(Map map) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, true, map.getName(),
                    map.getPacman_nb(), map.getGhost_nb());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                map.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Bot creation failed : no new id generated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

    }

    @Override
    public void update(Map map) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false, map.getName(),
                    map.getPacman_nb(), map.getGhost_nb(), map.getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Map map) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false, map.getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }
}
