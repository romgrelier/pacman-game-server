package server.service.pacman.dao.mariadb;

import server.service.pacman.beans.Game;
import server.service.pacman.beans.GamePlayedByPlayerAsGhost;
import server.service.pacman.beans.Player;
import server.service.pacman.dao.DAOException;
import server.service.pacman.dao.GamePlayedByPlayerAsGhostDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbGamePlayedByPlayerAsGhostDAO implements GamePlayedByPlayerAsGhostDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ALL = "SELECT * FROM Joint_player_ghost";
    private final String SQL_SELECT_BOT = "SELECT * FROM Joint_player_ghost WHERE id_player = ?";
    private final String SQL_SELECT_GAME = "SELECT * FROM Joint_player_ghost WHERE id_game = ?";

    private final String SQL_INSERT = "INSERT INTO Joint_player_ghost(id_game, id_player, score) VALUES(?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE Joint_player_ghost SET id_game = ?, id_player = ?, score = ? WHERE id_game = ?";
    private final String SQL_DELETE = "DELETE FROM Joint_player_ghost WHERE id_game = ?";

    private GamePlayedByPlayerAsGhost map(ResultSet resultSet) throws SQLException {
        GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = new GamePlayedByPlayerAsGhost();

        gamePlayedByPlayerAsGhost.setPlayer(daoFactory.getPlayerDAO().find(resultSet.getLong("id_player")));
        gamePlayedByPlayerAsGhost.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByPlayerAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsGhost;
    }

    private GamePlayedByPlayerAsGhost map(ResultSet resultSet, Player player) throws SQLException {
        GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = new GamePlayedByPlayerAsGhost();

        gamePlayedByPlayerAsGhost.setPlayer(player);
        gamePlayedByPlayerAsGhost.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByPlayerAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsGhost;
    }

    private GamePlayedByPlayerAsGhost map(ResultSet resultSet, Game game) throws SQLException {
        GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = new GamePlayedByPlayerAsGhost();

        gamePlayedByPlayerAsGhost.setPlayer(daoFactory.getPlayerDAO().find(resultSet.getLong("id_player")));
        gamePlayedByPlayerAsGhost.setGame(game);
        gamePlayedByPlayerAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByPlayerAsGhost;
    }

    public MariadbGamePlayedByPlayerAsGhostDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsGhost> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsGhost> gamePlayedByPlayerAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = map(resultSet);
                gamePlayedByPlayerAsGhosts.add(gamePlayedByPlayerAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsGhosts;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsGhost> findGame(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsGhost> gamePlayedByPlayerAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_BOT, false, player.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = map(resultSet, player);
                gamePlayedByPlayerAsGhosts.add(gamePlayedByPlayerAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsGhosts;
    }

    @Override
    public ArrayList<GamePlayedByPlayerAsGhost> findPlayer(Game game) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByPlayerAsGhost> gamePlayedByPlayerAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_GAME, false, game.getId());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost = map(resultSet, game);
                gamePlayedByPlayerAsGhosts.add(gamePlayedByPlayerAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByPlayerAsGhosts;
    }

    @Override
    public void create(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, false,
                    gamePlayedByPlayerAsGhost.getGame().getId(), gamePlayedByPlayerAsGhost.getPlayer().getId(),
                    gamePlayedByPlayerAsGhost.getScore());
            int status = preparedStatement.executeUpdate();

            if (status == 0) {
                throw new DAOException("Bot creation failed : no row added");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void update(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false,
                    gamePlayedByPlayerAsGhost.getGame().getId(), gamePlayedByPlayerAsGhost.getPlayer().getId(),
                    gamePlayedByPlayerAsGhost.getScore(), gamePlayedByPlayerAsGhost.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false,
                    gamePlayedByPlayerAsGhost.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

}
