package server.service.pacman.dao;

import server.service.pacman.dao.mariadb.MariadbDAOFactory;

public abstract class DAOFactory {
    public enum DataStorageType {
        MARIADB
    }

    public static DAOFactory getDaoFactory(DataStorageType type) {
        switch (type) {
        case MARIADB:
            return MariadbDAOFactory.getInstance();
        default:
            break;
        }
        return null;
    }

    public abstract GameDAO getGameDAO();

    public abstract MapDAO getMapDAO();

    public abstract BotDAO getBotDAO();

    public abstract PlayerDAO getPlayerDAO();

    public abstract GamePlayedByBotAsGhostDAO getGamePlayedByBotAsGhostDAO();

    public abstract GamePlayedByBotAsPacmanDAO getGamePlayedByBotAsPacmanDAO();

    public abstract GamePlayedByPlayerAsGhostDAO getGamePlayedByPlayerAsGhostDAO();

    public abstract GamePlayedByPlayerAsPacmanDAO getGamePlayedByPlayerAsPacmanDAO();
}
