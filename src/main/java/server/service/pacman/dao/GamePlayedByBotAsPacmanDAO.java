package server.service.pacman.dao;

import server.service.pacman.beans.Bot;
import server.service.pacman.beans.Game;
import server.service.pacman.beans.GamePlayedByBotAsPacman;

import java.util.ArrayList;

public interface GamePlayedByBotAsPacmanDAO {
    ArrayList<GamePlayedByBotAsPacman> findAll();

    ArrayList<GamePlayedByBotAsPacman> findGame(Bot bot);

    ArrayList<GamePlayedByBotAsPacman> findBot(Game game);

    void create(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);

    void update(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);

    void delete(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);
}
