package server.service.pacman.dao;

import server.service.pacman.beans.Game;

import java.util.ArrayList;

public interface GameDAO {
    Game find(Long id);

    ArrayList<Game> findAll();

    void create(Game game);

    void update(Game game);

    void delete(Game game);
}
