package server.service.pacman.beans;

import java.util.ArrayList;
import java.util.Date;

public class Game {
    private long id;
    private Map map;
    private java.util.Date date = new Date();
    private ArrayList<GamePlayedByPlayerAsPacman> playerPacman = new ArrayList<>();
    private ArrayList<GamePlayedByPlayerAsGhost> playerghost = new ArrayList<>();
    private ArrayList<GamePlayedByBotAsPacman> botPacman = new ArrayList<>();
    private ArrayList<GamePlayedByBotAsGhost> botGhost = new ArrayList<>();

    public Game() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<GamePlayedByPlayerAsPacman> getPlayerPacman() {
        return playerPacman;
    }

    public void setPlayerPacman(ArrayList<GamePlayedByPlayerAsPacman> playerPacman) {
        this.playerPacman = playerPacman;
    }

    public ArrayList<GamePlayedByPlayerAsGhost> getPlayerghost() {
        return playerghost;
    }

    public void setPlayerghost(ArrayList<GamePlayedByPlayerAsGhost> playerghost) {
        this.playerghost = playerghost;
    }

    public ArrayList<GamePlayedByBotAsPacman> getBotPacman() {
        return botPacman;
    }

    public void setBotPacman(ArrayList<GamePlayedByBotAsPacman> botPacman) {
        this.botPacman = botPacman;
    }

    public ArrayList<GamePlayedByBotAsGhost> getBotGhost() {
        return botGhost;
    }

    public void setBotGhost(ArrayList<GamePlayedByBotAsGhost> botGhost) {
        this.botGhost = botGhost;
    }
}
