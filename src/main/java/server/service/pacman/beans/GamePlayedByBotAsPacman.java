package server.service.pacman.beans;

public class GamePlayedByBotAsPacman {
    private Bot bot;
    private Game game;
    private int score;

    public GamePlayedByBotAsPacman() {
    }

    public Bot getBot() {
        return bot;
    }

    public void setBot(Bot bot) {
        this.bot = bot;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
