package server.service.pacman.beans;

public class GamePlayedByPlayerAsGhost {
    private Player player;
    private Game game;
    private int score;

    public GamePlayedByPlayerAsGhost() {
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
