package pacman;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {
    private final String logFile = "pacman.log";
    private static Logger logger = null;
    private PrintWriter writer;

    public Logger() {
        try {
            FileWriter fw = new FileWriter(logFile);
            writer = new PrintWriter(fw, true);
        } catch (IOException e) {

        }
    }

    public static Logger getInstance() {
        if (logger == null) {
            logger = new Logger();
        }

        return logger;
    }

    public void send(String tag, String message) {
        System.out.println("[" + tag + "] " + message);
        writer.println("[" + tag + "] " + message);
    }
}
