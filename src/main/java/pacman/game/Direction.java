package pacman.game;

public enum Direction {
    NORTH, SOUTH, EAST, WEST
}
