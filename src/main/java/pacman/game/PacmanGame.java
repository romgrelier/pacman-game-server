package pacman.game;

import pacman.Logger;
import pacman.game.actor.Actor;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;
import pacman.game.agent.Agent;
import pacman.game.agent.GhostAgent;
import pacman.game.agent.PacmanAgent;
import pacman.game.player.Player;
import pacman.observer.Observer;
import pacman.observer.Observer.Event;

import java.util.ArrayList;

public class PacmanGame extends Game {
    private String filename;
    private Maze maze;

    // Controls
    private ArrayList<Agent> agents = new ArrayList<>();
    private ArrayList<Player> players = new ArrayList<>();

    // Actors
    private ArrayList<Pacman> pacmans = new ArrayList<>();
    private ArrayList<Ghost> ghosts = new ArrayList<>();

    public PacmanGame(int maxTourCount) {
        super(maxTourCount);
    }

    public Maze getMaze() {
        return this.maze;
    }

    public ArrayList<Pacman> getPacmans() {
        return pacmans;
    }

    public ArrayList<Ghost> getGhosts() {
        return ghosts;
    }

    /**
     * Load the maze and add Actor to the game
     * 
     * @param filename contains the maze's layout
     * @throws Exception
     */
    public void changeMaze(String filename) throws Exception {
        agents.clear();
        pacmans.clear();
        ghosts.clear();

        this.filename = filename;
        maze = new Maze(filename);

        for (Position position : maze.getPacman_start()) {
            pacmans.add(new Pacman(position, this));
        }

        for (Position position : maze.getGhosts_start()) {
            ghosts.add(new Ghost(position, this));
        }

        addAgent();

        notifyObserver(Observer.Event.CHGMAZE);
    }

    /**
     * Take the control of an actor over an agent
     * 
     * @return new player created for controller
     */
    public Player addPlayer() {
        // TODO : add ghost control, choice with enum

        Player player = null;

        if (!agents.isEmpty()) {
            Actor actor = agents.get(0).getActor();
            agents.remove(0);
            player = new Player(this, actor);
        }

        players.add(player);

        return player;
    }

    /**
     * By default, all actors have an agent for control
     */
    public void addAgent() {
        for (Pacman pacman : pacmans) {
            agents.add(new PacmanAgent(pacman, this));
        }

        for (Ghost ghost : ghosts) {
            agents.add(new GhostAgent(ghost, this));
        }
    }

    /**
     * tour count reset to zero + init selected maze
     */
    @Override
    protected void initializeGame() {
        Logger.getInstance().send("PacmanGame", "Initialize Game");
        if (filename != null) {
            try {
                changeMaze(filename);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * get action from players and agent check if any death check end game
     * conditions
     */
    @Override
    protected void takeTurn() {
        // Logger.getInstance().send("PacmanGame", "takeTurn : " + tourCount + " / " +
        // maxTourCount);

        // get new commands
        for (Agent agent : agents) {
            if (agent.canPlay()) {
                agent.getActor().checkCollision();
                sendCommand(agent.getNextCommand(maze));
            }
        }

        for (Player player : players) {
            if (player.canPlay()) {
                player.getActor().checkCollision();
                sendCommand(player.getNextCommand(maze, this));
            }
        }
        /*
         * // check death for(int i = 0; i < ghosts.size(); ++i){
         * if(ghosts.get(i).isDead()){ //ghosts.remove(i);
         * maze.getGhosts_start().remove(i); } }
         * 
         * for(int i = 0; i < pacmans.size(); ++i){ if(pacmans.get(i).isDead()){
         * //pacmans.remove(i); maze.getPacman_start().remove(i); } }
         */
        if (pacmans.get(0).isDead()) {
            stop();
            notifyObserver(Event.END);
        }

        // TODO : add end condition : no more food
    }

    /**
     * Announce end of the game
     */
    @Override
    protected void gameOver() {
        Logger.getInstance().send("PacmanGame", "Game Over");
    }
}
