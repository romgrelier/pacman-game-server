package pacman.game;

import pacman.mvc.Model;
import pacman.observer.Observer;

public abstract class Game extends Model {
    protected boolean isRunning = true;
    protected int tourCount = 0;
    final protected int maxTourCount;

    private long speed = 200;

    public int getTourCount() {
        return tourCount;
    }

    public int getMaxTourCount() {
        return maxTourCount;
    }

    public void setSpeed(long speed) {
        this.speed = speed * 10;
    }

    protected Game(int maxTourCount) {
        this.maxTourCount = maxTourCount;
    }

    public void init() {
        tourCount = 0;
        initializeGame();
        notifyObserver(Observer.Event.UPDATE);
    }

    protected abstract void initializeGame();

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {
            step();

            try {
                Thread.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void step() {
        if (tourCount < maxTourCount) {
            ++tourCount;
            takeTurn();
        } else {
            gameOver();
            stop();
        }
        notifyObserver(Observer.Event.UPDATE);
    }

    protected abstract void takeTurn();

    public void stop() {
        isRunning = false;
    }

    protected abstract void gameOver();

}
