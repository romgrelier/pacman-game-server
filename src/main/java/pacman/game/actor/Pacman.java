package pacman.game.actor;

import pacman.Logger;
import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.state.pacman.AlivePacman;
import pacman.game.state.pacman.DeadPacman;
import pacman.game.state.pacman.InvinciblePacman;
import pacman.game.state.pacman.PacmanState;

public class Pacman extends Actor {
    private int score = 0;

    private PacmanState.StateName stateName = PacmanState.StateName.ALIVE;
    private PacmanState aliveState = new AlivePacman(this);
    private PacmanState deadState = new DeadPacman(this);
    private InvinciblePacman invincibleState = new InvinciblePacman(this);
    private PacmanState state = aliveState;

    public PacmanState.StateName getState() {
        return stateName;
    }

    public void switchAlive() {
        state = aliveState;
        stateName = PacmanState.StateName.ALIVE;
        dead = false;
        Logger.getInstance().send("PacmanAgent", "new state : ALIVE");
    }

    public void switchDead() {
        state = deadState;
        stateName = PacmanState.StateName.DEAD;
        dead = true;
        Logger.getInstance().send("PacmanAgent", "new state : DEAD");
    }

    public void switchInvincible() {
        state = invincibleState;
        stateName = PacmanState.StateName.INVINCIBLE;
        Logger.getInstance().send("PacmanAgent", "new state : INVINCIBLE");
    }

    public Pacman(Position position, PacmanGame pacmanGame) {
        super(position, pacmanGame);
    }

    public int getScore() {
        return score;
    }

    @Override
    public void checkCollision() {
        state.checkCollision(pacmanGame, position, pacmanGame.getGhosts());

        if (pacmanGame.getMaze().isFood(position.getX(), position.getY())) {
            pacmanGame.getMaze().setFood(position.getX(), position.getY(), false);
            ++score;
        }
    }
}
