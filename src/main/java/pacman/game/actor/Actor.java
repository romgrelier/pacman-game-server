package pacman.game.actor;

import pacman.game.Direction;
import pacman.game.PacmanGame;
import pacman.game.Position;

public abstract class Actor {
    protected Position position;
    protected PacmanGame pacmanGame;
    protected boolean dead = false;

    public Actor(Position position, PacmanGame pacmanGame) {
        this.position = position;
        this.pacmanGame = pacmanGame;
    }

    public Position getPosition() {
        return position;
    }

    public void move(Direction direction) {
        position.setDirection(direction);
        position.moveForward();

        checkCollision();
    }

    public boolean isDead() {
        return dead;
    }

    public abstract void checkCollision();
}
