package pacman.game.actor;

import pacman.Logger;
import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.state.ghost.AliveGhost;
import pacman.game.state.ghost.DeadGhost;
import pacman.game.state.ghost.GhostState;
import pacman.game.state.ghost.VulnerableGhost;

public class Ghost extends Actor {
    private GhostState aliveGhost = new AliveGhost(this);
    private GhostState deadGhost = new DeadGhost(this);
    private GhostState vulnerableGhost = new VulnerableGhost(this);
    private GhostState state = aliveGhost;
    private GhostState.StateName stateName = GhostState.StateName.ALIVE;

    public void switchAlive() {
        state = aliveGhost;
        stateName = GhostState.StateName.ALIVE;
        Logger.getInstance().send("GhostAgent", "new state : ALIVE");
    }

    public void switchDead() {
        state = deadGhost;
        stateName = GhostState.StateName.DEAD;
        dead = true;
        Logger.getInstance().send("GhostAgent", "new state : DEAD");
    }

    public void switchVulnerable() {
        state = vulnerableGhost;
        stateName = GhostState.StateName.VULNERABLE;
        Logger.getInstance().send("GhostAgent", "new state : VULNERABLE");
    }

    public GhostState.StateName getState() {
        return stateName;
    }

    public Ghost(Position position, PacmanGame pacmanGame) {
        super(position, pacmanGame);
    }

    @Override
    public void checkCollision() {
        state.checkCollision(pacmanGame, position, pacmanGame.getPacmans());
    }
}
