package pacman.game.player;

import pacman.command.Command;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.actor.Pacman;
import pacman.game.strategy.Dummy;

public class PlayerPacman extends Player {
    private Pacman pacman;

    private Dummy dummy = new Dummy();

    public PlayerPacman(PacmanGame pacmanGame, Pacman pacman) {
        super(pacmanGame);
        this.pacman = pacman;
        strategy = dummy;
    }

    public Dummy getDummy() {
        return dummy;
    }

    @Override
    public Command getNextCommand(Maze maze, PacmanGame pacmanGame) {
        moveCommand.setInvoker(pacman);
        moveCommand.setMaze(maze);
        moveCommand.setDirection(dummy.getNextDirection(pacman.getPosition(), pacmanGame));
        return moveCommand;
    }

    @Override
    public boolean canPlay() {
        return !pacman.isDead();
    }

    @Override
    public Actor getActor() {
        return pacman;
    }

}
