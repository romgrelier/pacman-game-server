package pacman.game.player;

import pacman.command.Command;
import pacman.command.game.MoveCommand;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.strategy.Dummy;

public class Player {
    protected Dummy strategy = new Dummy();
    protected PacmanGame pacmanGame;
    protected MoveCommand moveCommand = new MoveCommand();
    protected Actor actor;

    public Player(PacmanGame pacmanGame) {
        this.pacmanGame = pacmanGame;
    }

    public Player(PacmanGame pacmanGame, Actor actor) {
        this.actor = actor;
        this.pacmanGame = pacmanGame;
    }

    public Command getNextCommand(Maze maze, PacmanGame pacmanGame) {
        moveCommand.setInvoker(actor);
        moveCommand.setMaze(maze);
        moveCommand.setDirection(strategy.getNextDirection(actor.getPosition(), pacmanGame));
        return moveCommand;
    }

    public boolean canPlay() {
        return !actor.isDead();
    }

    public Actor getActor() {
        return actor;
    }

    public Dummy getDummy() {
        return strategy;
    }
}
