package pacman.game.state.ghost;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;

import java.util.ArrayList;

public class DeadGhost extends GhostState {
    public DeadGhost(Ghost ghost) {
        super(ghost);
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Pacman> pacmans) {

    }
}
