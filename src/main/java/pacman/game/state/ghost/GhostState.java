package pacman.game.state.ghost;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;
import pacman.game.state.State;

import java.util.ArrayList;

public abstract class GhostState implements State {
    public enum StateName {
        ALIVE, DEAD, VULNERABLE
    }

    protected Ghost ghost;

    public GhostState(Ghost ghost) {
        this.ghost = ghost;
    }

    public abstract void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Pacman> pacmans);
}
