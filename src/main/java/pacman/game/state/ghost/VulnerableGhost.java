package pacman.game.state.ghost;

import pacman.Logger;
import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;

import java.util.ArrayList;

public class VulnerableGhost extends GhostState {
    private final int INITIAL_COUNTER = 20;
    private int counter = INITIAL_COUNTER;

    public VulnerableGhost(Ghost ghost) {
        super(ghost);
    }

    public void reset() {
        counter = INITIAL_COUNTER;
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Pacman> pacmans) {
        --counter;
        // Logger.getInstance().send("VulnerableGhost", "counter = " + counter);
        if (counter <= 0) {
            reset();
            ghost.switchAlive();
        } else {
            for (Pacman pacman : pacmans) {
                if (pacman.getPosition().equal(ghost.getPosition())) {
                    ghost.switchDead();
                }
            }
        }
    }
}
