package pacman.game.state.ghost;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;
import pacman.game.state.pacman.PacmanState;

import java.util.ArrayList;

public class AliveGhost extends GhostState {
    public AliveGhost(Ghost ghost) {
        super(ghost);
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Pacman> pacmans) {
        for (Pacman pacman : pacmans) {
            if (pacman.getState() == PacmanState.StateName.INVINCIBLE) {
                ghost.switchVulnerable();
            }
        }
    }
}
