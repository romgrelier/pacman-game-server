package pacman.game.state.pacman;

import pacman.Logger;
import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;

import java.util.ArrayList;

public class InvinciblePacman extends PacmanState {
    private final int INITIAL_COUNTER = 20;
    private int counter = INITIAL_COUNTER;

    public InvinciblePacman(Pacman pacman) {
        super(pacman);
    }

    public void reset() {
        this.counter = INITIAL_COUNTER;
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Ghost> ghosts) {
        --counter;
        // Logger.getInstance().send("InvinciblePacman", "counter = " + counter);
        if (counter <= 0) {
            reset();
            pacman.switchAlive();
        }
    }
}
