package pacman.game.state.pacman;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;
import pacman.game.state.State;

import java.util.ArrayList;

public abstract class PacmanState implements State {
    public enum StateName {
        ALIVE, DEAD, INVINCIBLE
    }

    protected Pacman pacman;

    public PacmanState(Pacman pacman) {
        this.pacman = pacman;
    }

    public abstract void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Ghost> ghosts);
}
