package pacman.game.state.pacman;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Actor;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;

import java.util.ArrayList;

public class DeadPacman extends PacmanState {
    private Actor actor;

    public DeadPacman(Pacman pacman) {
        super(pacman);
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Ghost> ghosts) {

    }
}
