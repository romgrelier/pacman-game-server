package pacman.game.state.pacman;

import pacman.game.PacmanGame;
import pacman.game.Position;
import pacman.game.actor.Ghost;
import pacman.game.actor.Pacman;

import java.util.ArrayList;

public class AlivePacman extends PacmanState {
    public AlivePacman(Pacman pacman) {
        super(pacman);
    }

    @Override
    public void checkCollision(PacmanGame pacmanGame, Position position, ArrayList<Ghost> ghosts) {
        if (pacmanGame.getMaze().isCapsule(position.getX(), position.getY())) {
            pacmanGame.getMaze().setCapsule(position.getX(), position.getY(), false);
            pacman.switchInvincible();
        }

        for (Ghost ghost : ghosts) {
            if (ghost.getPosition().equal(pacman.getPosition())) {
                pacman.switchDead();
            }
        }
    }

}
