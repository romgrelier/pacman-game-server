package pacman.game;

import pacman.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

public class Maze implements Serializable {

	public enum MazeElement {
		EMPTY, WALL, FOOD, CAPSULE, PACMAN, GHOST
	}

	private int size_x;
	private int size_y;

	private ArrayList<Position> pacmanPosition = new ArrayList<>();
	private ArrayList<Position> ghostPosition = new ArrayList<>();

	/**
	 * Les elements du labyrinthe
	 */
	private MazeElement[][] maze;

	public Maze(String filename) throws Exception {
		try {
			Logger.getInstance().send("Maze", "Layout file is " + filename);

			// Lecture du fichier pour determiner la taille du labyrinthe
			InputStream ips = new FileInputStream(filename);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			int nbX = 0;
			int nbY = 0;
			while ((ligne = br.readLine()) != null) {
				ligne = ligne.trim();
				if (nbX == 0) {
					nbX = ligne.length();
				} else if (nbX != ligne.length())
					throw new Exception("Wrong Input Format: all lines must have the same size");
				nbY++;
			}
			br.close();
			Logger.getInstance().send("Maze", "### Size of maze is " + nbX + ";" + nbY);

			// Initialisation du labyrinthe
			size_x = nbX;
			size_y = nbY;
			maze = new MazeElement[size_x][size_y];

			// Lecture du fichier pour mettre a jour le labyrinthe
			ips = new FileInputStream(filename);
			ipsr = new InputStreamReader(ips);
			br = new BufferedReader(ipsr);
			int y = 0;
			while ((ligne = br.readLine()) != null) {
				ligne = ligne.trim();

				for (int x = 0; x < ligne.length(); x++) {
					switch (ligne.charAt(x)) {
					case '%':
						maze[x][y] = MazeElement.WALL;
						break;
					case '.':
						maze[x][y] = MazeElement.FOOD;
						break;
					case 'o':
						maze[x][y] = MazeElement.CAPSULE;
						break;
					case 'P':
						pacmanPosition.add(new Position(x, y, Direction.EAST));
						// maze[x][y] = MazeElement.PACMAN;
						break;
					case 'G':
						ghostPosition.add(new Position(x, y, Direction.EAST));
						// maze[x][y] = MazeElement.GHOST;
						break;
					default:
						maze[x][y] = MazeElement.EMPTY;
						break;
					}
				}
				y++;
			}
			br.close();

			if (pacmanPosition.size() == 0)
				throw new Exception("Wrong input format: must specify a PacmanAgent start");

			// On verifie que le labyrinthe est clos
			for (int x = 0; x < size_x; x++)
				if (maze[x][0] != MazeElement.WALL)
					throw new Exception("Wrong input format: the maze must be closed");
			for (int x = 0; x < size_x; x++)
				if (maze[x][size_y - 1] != MazeElement.WALL)
					throw new Exception("Wrong input format: the maze must be closed");
			for (y = 0; y < size_y; y++)
				if (maze[0][y] != MazeElement.WALL)
					throw new Exception("Wrong input format: the maze must be closed");
			for (y = 0; y < size_y; y++)
				if (maze[size_x - 1][0] != MazeElement.WALL)
					throw new Exception("Wrong input format: the maze must be closed");
			Logger.getInstance().send("Maze", "### Maze loaded.\"");

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Probleme a la lecture du fichier: " + e.getMessage());
		}
	}

	public void setMaze(MazeElement[][] maze) {
		this.maze = maze;
	}

	public MazeElement[][] getMaze() {
		return this.maze;
	}

	/**
	 * Renvoie la taille X du labyrtinhe
	 */
	public int getSizeX() {
		return (size_x);
	}

	/**
	 * Renvoie la taille Y du labyrinthe
	 */
	public int getSizeY() {
		return (size_y);
	}

	/**
	 * Permet de savoir si il y a un mur
	 */
	public boolean isWall(int x, int y) {
		assert ((x >= 0) && (x < size_x));
		assert ((y >= 0) && (y < size_y));
		return maze[x][y] == MazeElement.WALL;
	}

	/**
	 * Permet de savoir si il y a de la nourriture
	 */
	public boolean isFood(int x, int y) {
		assert ((x >= 0) && (x < size_x));
		assert ((y >= 0) && (y < size_y));
		return maze[x][y] == MazeElement.FOOD;
	}

	public void setFood(int x, int y, boolean b) {
		if (b) {
			maze[x][y] = MazeElement.FOOD;
		} else {
			maze[x][y] = MazeElement.EMPTY;
		}
	}

	/**
	 * Permet de savoir si il y a une capsule
	 */
	public boolean isCapsule(int x, int y) {
		assert ((x >= 0) && (x < size_x));
		assert ((y >= 0) && (y < size_y));
		return maze[x][y] == MazeElement.CAPSULE;
	}

	public void setCapsule(int x, int y, boolean b) {
		if (b) {
			maze[x][y] = MazeElement.CAPSULE;
		} else {
			maze[x][y] = MazeElement.EMPTY;
		}
	}

	public MazeElement getElement(int x, int y) {
		return maze[x][y];
	}

	public void setMazeElement(int x, int y, MazeElement e) {
		maze[x][y] = e;
	}

	/**
	 * Renvoie le nombre de pacmans
	 * 
	 * @return
	 */
	public int getInitNumberOfPacmans() {
		return (pacmanPosition.size());
	}

	/**
	 * Renvoie le nombre de fantomes
	 * 
	 * @return
	 */
	public int getInitNumberOfGhosts() {
		return (ghostPosition.size());
	}

	public ArrayList<Position> getPacman_start() {
		return pacmanPosition;
	}

	public void setPacman_start(ArrayList<Position> pacman_start) {
		this.pacmanPosition = pacman_start;
	}

	public ArrayList<Position> getGhosts_start() {
		return ghostPosition;
	}

	public void setGhosts_start(ArrayList<Position> ghosts_start) {
		this.ghostPosition = ghosts_start;
	}

}
