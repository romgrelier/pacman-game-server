package pacman.game.strategy;

import pacman.Logger;
import pacman.core.Coordonate2D;
import pacman.game.Direction;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Gluttony implements Strategy {
    private ArrayList<Direction> directions = new ArrayList<>();

    private ArrayList<Coordonate2D> searchFood(Coordonate2D crawler, Maze maze, ArrayList<Coordonate2D> visited) {
        ArrayList<Coordonate2D> path = new ArrayList<>();

        if (visited.contains(crawler)) {
            return path;
        }

        visited.add(crawler);

        if (maze.isFood(crawler.x, crawler.y) || maze.isCapsule(crawler.x, crawler.y)) {
            path.add(crawler);
            return path;
        }

        ArrayList<Coordonate2D> availablePath;

        if (!maze.isWall(crawler.x + 1, crawler.y)) {
            availablePath = searchFood(new Coordonate2D(crawler.x + 1, crawler.y), maze, visited);
            if (availablePath.isEmpty()) {
                return path;
            } else {
                path.add(crawler);
                return path;
            }
        }
        if (!maze.isWall(crawler.x - 1, crawler.y)) {
            availablePath = searchFood(new Coordonate2D(crawler.x - 1, crawler.y), maze, visited);
            if (availablePath.isEmpty()) {
                return path;
            } else {
                path.add(crawler);
                return path;
            }
        }
        if (!maze.isWall(crawler.x, crawler.y + 1)) {
            availablePath = searchFood(new Coordonate2D(crawler.x, crawler.y + 1), maze, visited);
            if (availablePath.isEmpty()) {
                return path;
            } else {
                path.add(crawler);
                return path;
            }
        }
        if (!maze.isWall(crawler.x, crawler.y - 1)) {
            availablePath = searchFood(new Coordonate2D(crawler.x, crawler.y - 1), maze, visited);
            if (availablePath.isEmpty()) {
                return path;
            } else {
                path.add(crawler);
                return path;
            }
        }

        // Collections.reverse(path);

        return path;
    }

    @Override
    public Direction getNextDirection(Position position, PacmanGame pacmanGame) {
        if (directions.isEmpty()) {
            ArrayList<Coordonate2D> visited = new ArrayList<>();

            ArrayList<Coordonate2D> path = searchFood(new Coordonate2D(position.getX(), position.getY()),
                    pacmanGame.getMaze(), visited);

            Coordonate2D origin = new Coordonate2D(position.getX(), position.getY());
            Coordonate2D actual = new Coordonate2D(0, 0);

            Logger.getInstance().send("Gluttony", "path finished : " + path.size());

            while (!origin.equals(actual)) {
                Coordonate2D inspect = path.remove(0);

                Logger.getInstance().send("Gluttony", "building " + inspect);

                if (actual.x < inspect.x && actual.y == inspect.y) {
                    directions.add(Direction.WEST);
                } else if (actual.x > inspect.x && actual.y == inspect.y) {
                    directions.add(Direction.EAST);
                } else if (actual.x == inspect.x && actual.y > inspect.y) {
                    directions.add(Direction.NORTH);
                } else if (actual.x == inspect.x && actual.y < inspect.y) {
                    directions.add(Direction.SOUTH);
                } else {
                    Random random = new Random();
                    int choice = random.nextInt(4);

                    switch (choice) {
                    case 0:
                        directions.add(Direction.NORTH);
                        break;
                    case 1:
                        directions.add(Direction.SOUTH);
                        break;
                    case 2:
                        directions.add(Direction.EAST);
                        break;
                    case 3:
                        directions.add(Direction.WEST);
                        break;
                    }
                }

                actual = inspect;
            }

            Logger.getInstance().send("Gluttony", "directions finished");
        }

        return directions.remove(0);
    }
}
