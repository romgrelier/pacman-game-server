package pacman.game.strategy;

import pacman.Logger;
import pacman.core.Coordonate2D;
import pacman.game.Direction;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HuntPacman implements Strategy {
    public class Node {
        int cost;
        int heuristic;
        Coordonate2D predecessor;

        Node(int cost, int heuristic) {
            this.cost = cost;
            this.heuristic = heuristic;
            predecessor = null;
        }
    }

    private Node[][] graph;
    private int sizeX;
    private int sizeY;

    private ArrayList<Direction> path = new ArrayList<>();

    // avoid to go where pacman is no more at the end of the path
    private int entropy = 0;

    private void buildGraph(Position target, Maze maze) {
        graph = new Node[maze.getSizeX()][maze.getSizeY()];
        sizeX = maze.getSizeX();
        sizeY = maze.getSizeY();

        for (int i = 0; i < sizeX; ++i) {
            for (int j = 0; j < sizeY; ++j) {
                if (!maze.isWall(i, j)) {
                    graph[i][j] = new Node(0, Math.abs(i - target.getX()) + Math.abs(j - target.getY()));
                }
            }
        }
    }

    @Override
    public String toString() {
        String string = "\n";

        string += "[HEURISTIC]\n";
        for (int j = 0; j < sizeY; ++j) {
            for (int i = 0; i < sizeX; ++i) {
                if (graph[i][j] != null) {
                    if (graph[i][j].heuristic == 0) {
                        string += "\tX";
                    } else {
                        string += "\t" + graph[i][j].heuristic;
                    }
                } else {
                    string += "\t[]";
                }
            }
            string += "\n";
        }

        string += "[COST]\n";
        for (int j = 0; j < sizeY; ++j) {
            for (int i = 0; i < sizeX; ++i) {
                if (graph[i][j] != null) {
                    string += "\t" + graph[i][j].cost;
                } else {
                    string += "\t[]";
                }
            }
            string += "\n";
        }

        return string;
    }

    private ArrayList<Coordonate2D> getNeighborhood(Coordonate2D position) {
        ArrayList<Coordonate2D> neighborhood = new ArrayList<>();
        int x = position.x;
        int y = position.y;

        if (graph[x + 1][y] != null) {
            neighborhood.add(new Coordonate2D(x + 1, y));
        }
        if (graph[x - 1][y] != null) {
            neighborhood.add(new Coordonate2D(x - 1, y));
        }
        if (graph[x][y + 1] != null) {
            neighborhood.add(new Coordonate2D(x, y + 1));
        }
        if (graph[x][y - 1] != null) {
            neighborhood.add(new Coordonate2D(x, y - 1));
        }

        return neighborhood;
    }

    private void buildPath(Position origin, Position target) {
        ArrayList<Coordonate2D> active = new ArrayList<>();
        ArrayList<Coordonate2D> inactive = new ArrayList<>();

        Coordonate2D targetC = new Coordonate2D(target.getX(), target.getY());
        Coordonate2D actual = new Coordonate2D(origin.getX(), origin.getY());
        active.add(actual);

        // compute cost
        while (!active.isEmpty() && !targetC.equals(actual)) {
            actual = active.remove(0); // the first should be the lower cost + heuristic (sorted list)
            inactive.add(actual);

            for (Coordonate2D n : getNeighborhood(actual)) {
                if (!inactive.contains(n) && !active.contains(n)
                        || graph[n.x][n.y].cost > (graph[actual.x][actual.y].cost + 1)) {

                    graph[n.x][n.y].cost = graph[actual.x][actual.y].cost + 1;
                    graph[n.x][n.y].predecessor = new Coordonate2D(actual.x, actual.y);
                    active.add(n);
                    active.sort(new Comparator<Coordonate2D>() {
                        @Override
                        public int compare(Coordonate2D coordonate2D, Coordonate2D t1) {
                            if (graph[coordonate2D.x][coordonate2D.y].cost
                                    + graph[coordonate2D.x][coordonate2D.y].heuristic > graph[t1.x][t1.y].cost
                                            + graph[t1.x][t1.y].heuristic) {
                                return 1;
                            } else if (graph[coordonate2D.x][coordonate2D.y].cost
                                    + graph[coordonate2D.x][coordonate2D.y].heuristic < graph[t1.x][t1.y].cost
                                            + graph[t1.x][t1.y].heuristic) {
                                return -1;
                            } else {
                                if (graph[coordonate2D.x][coordonate2D.y].cost > graph[t1.x][t1.y].cost) {
                                    return 1;
                                } else if (graph[coordonate2D.x][coordonate2D.y].cost < graph[t1.x][t1.y].cost) {
                                    return -1;
                                }
                            }
                            return 0;
                        }
                    });
                }
            }
        }

        if (graph[targetC.x][targetC.y].cost == 0) {
            path.add(Direction.SOUTH);
            Logger.getInstance().send("HuntPacman", "no path available");
        } else {
            // build path
            actual = targetC;
            while (!actual.equals(origin.getX(), origin.getY())) {
                Coordonate2D inspect = graph[actual.x][actual.y].predecessor;

                if (inspect.x < actual.x && inspect.y == actual.y) {
                    path.add(Direction.EAST);
                } else if (inspect.x > actual.x && inspect.y == actual.y) {
                    path.add(Direction.WEST);
                } else if (inspect.x == actual.x && inspect.y < actual.y) {
                    path.add(Direction.SOUTH);
                } else if (inspect.x == actual.x && inspect.y > actual.y) {
                    path.add(Direction.NORTH);
                }

                actual = inspect;
            }
        }

        Collections.reverse(path);
    }

    @Override
    public Direction getNextDirection(Position position, PacmanGame pacmanGame) {
        if (entropy == 0) {
            path.clear();
        }

        if (path.isEmpty()) {
            if (!pacmanGame.getPacmans().isEmpty()) {
                Position target = pacmanGame.getPacmans().get(0).getPosition();

                buildGraph(target, pacmanGame.getMaze());

                buildPath(position, target);

                entropy = Math.abs(path.size() / 2);
            }
            path.add(Direction.SOUTH);
        }

        --entropy;

        // Logger.getInstance().send("HuntPacman", this.toString());

        return path.remove(0);
    }
}
