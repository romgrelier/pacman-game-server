package pacman.game.strategy;

import pacman.game.Direction;
import pacman.game.PacmanGame;
import pacman.game.Position;

public class Dummy implements Strategy {
    private Direction direction = Direction.EAST;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public Direction getNextDirection(Position position, PacmanGame pacmanGame) {
        return direction;
    }
}
