package pacman.game.strategy;

import pacman.game.Direction;
import pacman.game.PacmanGame;
import pacman.game.Position;

public interface Strategy {
    Direction getNextDirection(Position position, PacmanGame pacmanGame);
}
