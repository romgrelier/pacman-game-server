package pacman.game.strategy;

import pacman.Logger;
import pacman.game.Direction;
import pacman.game.PacmanGame;
import pacman.game.Position;

import java.util.Random;

public class RandomMove implements Strategy {
    @Override
    public Direction getNextDirection(Position position, PacmanGame pacmanGame) {
        Random random = new Random();
        int choice = random.nextInt(4);

        Direction direction = Direction.WEST;
        switch (choice) {
        case 0:
            direction = Direction.NORTH;
            break;
        case 1:
            direction = Direction.SOUTH;
            break;
        case 2:
            direction = Direction.EAST;
            break;
        }

        // Logger.getInstance().send("RandomMove", direction.toString());

        return direction;
    }
}
