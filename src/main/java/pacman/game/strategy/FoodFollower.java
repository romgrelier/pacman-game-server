package pacman.game.strategy;

import pacman.game.Direction;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.Position;

import java.util.ArrayList;

public class FoodFollower implements Strategy {

    private ArrayList<Direction> getAvailableMove(Position position, Maze maze) {
        ArrayList<Direction> available = new ArrayList<>();

        if (!maze.isWall(position.getX(), position.getY() - 1)) {
            available.add(Direction.NORTH);
        }
        if (!maze.isWall(position.getX(), position.getY() + 1)) {
            available.add(Direction.SOUTH);
        }
        if (!maze.isWall(position.getX() - 1, position.getY())) {
            available.add(Direction.WEST);
        }
        if (!maze.isWall(position.getX() + 1, position.getY())) {
            available.add(Direction.EAST);
        }

        return available;
    }

    @Override
    public Direction getNextDirection(Position position, PacmanGame pacmanGame) {
        Maze maze = pacmanGame.getMaze();
        Direction direction = Direction.WEST;

        for (Direction d : getAvailableMove(position, maze)) {
            switch (d) {
            case NORTH:
                if (maze.isFood(position.getX(), position.getY() - 1)) {
                    direction = Direction.NORTH;
                }
                break;
            case SOUTH:
                if (maze.isFood(position.getX(), position.getY() + 1)) {
                    direction = Direction.SOUTH;
                }
                break;
            case EAST:
                if (maze.isFood(position.getX() + 1, position.getY())) {
                    direction = Direction.EAST;
                }
                break;
            case WEST:
                if (maze.isFood(position.getX() - 1, position.getY())) {
                    direction = Direction.WEST;
                }
                break;
            }
        }

        return direction;
    }
}
