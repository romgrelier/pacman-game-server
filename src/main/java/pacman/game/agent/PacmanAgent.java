package pacman.game.agent;

import pacman.command.Command;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.actor.Pacman;
import pacman.game.state.pacman.PacmanState;
import pacman.game.strategy.FoodFollower;
import pacman.game.strategy.Gluttony;
import pacman.game.strategy.RandomMove;
import pacman.game.strategy.Strategy;

public class PacmanAgent extends Agent {
    private Pacman pacman;
    private PacmanGame pacmanGame;

    private Strategy randomMove = new RandomMove();
    private Strategy foodFollower = new FoodFollower();
    private Strategy gluttony = new Gluttony();
    private Strategy strategy = gluttony;

    private int score = 0;

    public PacmanAgent(Pacman pacman, PacmanGame pacmanGame) {
        super(pacmanGame);
        this.pacman = pacman;
        this.pacmanGame = pacmanGame;
    }

    @Override
    public boolean canPlay() {
        return !pacman.isDead();
    }

    @Override
    public Command getNextCommand(Maze maze) {
        // TODO : switch strategy with the actual state of the game
        switch (pacman.getState()) {
        case ALIVE:
            // TODO : avoid ghost and search food
            strategy = foodFollower;
            break;
        case DEAD:
            // TODO : do nothing
            break;
        case INVINCIBLE:
            // TODO : hunt ghosts
            strategy = randomMove;
            break;
        }

        if (score == pacman.getScore()) {
            strategy = randomMove;
        }

        moveCommand.setInvoker(pacman);
        moveCommand.setMaze(maze);
        moveCommand.setDirection(strategy.getNextDirection(pacman.getPosition(), pacmanGame));
        return moveCommand;
    }

    @Override
    public Actor getActor() {
        return pacman;
    }
}
