package pacman.game.agent;

import pacman.command.Command;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.actor.Ghost;
import pacman.game.strategy.HuntPacman;
import pacman.game.strategy.RandomMove;
import pacman.game.strategy.Strategy;

public class GhostAgent extends Agent {
    private Ghost ghost;

    private Strategy randomMove = new RandomMove();
    private Strategy huntPacman = new HuntPacman();

    public GhostAgent(Ghost ghost, PacmanGame pacmanGame) {
        super(pacmanGame);
        this.ghost = ghost;
        strategy = huntPacman;
    }

    @Override
    public boolean canPlay() {
        return !ghost.isDead();
    }

    @Override
    public Command getNextCommand(Maze maze) {
        // TODO : switch strategy with the actual state of the game
        switch (ghost.getState()) {
        case ALIVE:
            // TODO : avoid ghost and search food
            strategy = huntPacman;
            break;
        case VULNERABLE:
            // TODO : avoid pacman
            strategy = randomMove;
            break;
        }

        if (pacmanGame.getPacmans().isEmpty()) {
            strategy = randomMove;
        }

        moveCommand.setInvoker(ghost);
        moveCommand.setMaze(maze);
        moveCommand.setDirection(strategy.getNextDirection(ghost.getPosition(), pacmanGame));
        return moveCommand;
    }

    @Override
    public Actor getActor() {
        return ghost;
    }
}
