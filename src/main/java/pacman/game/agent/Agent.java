package pacman.game.agent;

import pacman.command.Command;
import pacman.command.game.MoveCommand;
import pacman.game.Maze;
import pacman.game.PacmanGame;
import pacman.game.actor.Actor;
import pacman.game.strategy.Strategy;

public abstract class Agent {
    protected Strategy strategy;
    protected PacmanGame pacmanGame;
    protected MoveCommand moveCommand = new MoveCommand();

    public Agent(PacmanGame pacmanGame) {
        this.pacmanGame = pacmanGame;
    }

    public abstract Command getNextCommand(Maze maze);

    public abstract boolean canPlay();

    public abstract Actor getActor();
}
