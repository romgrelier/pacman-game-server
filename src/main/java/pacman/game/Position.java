package pacman.game;

public class Position {
    private int x;
    private int y;
    private Direction direction;

    public Position(Position position) {
        this.x = position.x;
        this.y = position.y;
        this.direction = position.direction;
    }

    public Position(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean equal(Position position) {
        return this.x == position.x && this.y == position.y;
    }

    public void moveForward() {
        switch (direction) {
        case NORTH:
            --y;
            break;
        case SOUTH:
            ++y;
            break;
        case EAST:
            ++x;
            break;
        case WEST:
            --x;
            break;
        }
    }

    @Override
    public String toString() {
        return "[" + direction + " | X : " + x + " |  Y : " + y + "]";
    }
}
