package pacman.controller;

import pacman.Logger;
import pacman.command.CommandReceiver;
import pacman.command.game.*;
import pacman.mvc.Controller;

public class BasicController extends Controller {
    private ResetCommand resetCommand = new ResetCommand();
    private RunCommand runCommand = new RunCommand();
    private StepCommand stepCommand = new StepCommand();
    private PauseCommand pauseCommand = new PauseCommand();
    private QuitCommand quitCommand = new QuitCommand();
    private SetSpeed setSpeedCommand = new SetSpeed();
    private SetMaze setMazeCommand = new SetMaze();

    @Override
    public void sendInput(Input input) {
        switch (input) {
        case RESET:
            receiver.sendCommand(resetCommand);
            break;
        case RUN:
            receiver.sendCommand(runCommand);
            break;
        case STEP:
            receiver.sendCommand(stepCommand);
            break;
        case PAUSE:
            receiver.sendCommand(pauseCommand);
            break;
        case QUIT:
            receiver.sendCommand(quitCommand);
            break;
        default:
            Logger.getInstance().send("BasicController", "Not handled input in Basic Controller");
            break;
        }
    }

    public void sendInput(Input input, long number) {
        switch (input) {
        case SETSPEED:
            setSpeedCommand.setSpeed(number);
            receiver.sendCommand(setSpeedCommand);
            break;
        default:
            Logger.getInstance().send("BasicController", "Not handled input in Basic Controller");
            break;
        }
    }

    public void sendInput(Input input, String string) {
        switch (input) {
        case CHGMAZE:
            setMazeCommand.setFile(string);
            receiver.sendCommand(setMazeCommand);
            break;
        default:
            Logger.getInstance().send("BasicController", "Not handled input in Basic Controller");
            break;
        }
    }

    @Override
    public void attachReceiver(CommandReceiver receiver) {
        this.receiver = receiver;
    }
}
