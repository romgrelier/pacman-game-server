package pacman.controller;

import pacman.Logger;
import pacman.command.Command;
import pacman.command.CommandReceiver;
import pacman.command.game.*;
import pacman.game.Direction;
import pacman.game.player.Player;

public class ServerController extends NetworkController {
    private Command resetCommand = new ResetCommand();
    private Command runCommand = new RunCommand();
    private Command stepCommand = new StepCommand();
    private Command pauseCommand = new PauseCommand();
    private Command quitCommand = new QuitCommand();
    private SetSpeed setSpeedCommand = new SetSpeed();
    private SetMaze setMazeCommand = new SetMaze();

    private InputMoveCommand inputMoveCommand = new InputMoveCommand();
    private RequestPlayerCreate requestPlayerCreate = new RequestPlayerCreate();

    private Player player1 = null;
    private Player player2 = null;
    private Player player3 = null;

    private void sendMoveCommand(Player player, Direction direction) {
        inputMoveCommand.setDirection(direction);
        inputMoveCommand.setPlayer(player);
        receiver.sendCommand(inputMoveCommand);
    }

    public void setPlayer(int id, Player player) {
        switch (id) {
        case 0:
            player1 = player;
            break;
        case 1:
            player2 = player;
            break;
        case 2:
            player3 = player;
            break;
        }
    }

    private void sendRequestPlayer(int id) {
        requestPlayerCreate.setController(this);
        requestPlayerCreate.setId(id);
        receiver.sendCommand(requestPlayerCreate);
    }

    @Override
    public void sendInput(Input input) {

        switch (input) {
        case Z:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.NORTH);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case Q:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.WEST);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case S:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.SOUTH);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case D:
            if (player1 != null) {
                sendMoveCommand(player1, Direction.EAST);
            } else {
                sendRequestPlayer(0);
            }
            break;
        case I:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.NORTH);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case J:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.WEST);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case K:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.SOUTH);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case L:
            if (player2 != null) {
                sendMoveCommand(player2, Direction.EAST);
            } else {
                sendRequestPlayer(1);
            }
            break;
        case RESET:
            player1 = null;
            player2 = null;
            player3 = null;
            receiver.sendCommand(resetCommand);
            break;
        case RUN:
            receiver.sendCommand(runCommand);
            break;
        case STEP:
            receiver.sendCommand(stepCommand);
            break;
        case PAUSE:
            receiver.sendCommand(pauseCommand);
            break;
        case QUIT:
            receiver.sendCommand(quitCommand);
            break;
        case SETSPEED:
            break;
        case CHGMAZE:
            break;
        default:
            Logger.getInstance().send("AdvancedController", "Not handled input in Advanced Controller");
            break;
        }
    }

    public void sendInput(Input input, long number) {
        switch (input) {
        case SETSPEED:
            setSpeedCommand.setSpeed(number);
            receiver.sendCommand(setSpeedCommand);
            break;
        default:
            Logger.getInstance().send("AdvancedController", "Not handled input in Advanced Controller");
            break;
        }
    }

    public void sendInput(Input input, String string) {
        switch (input) {
        case CHGMAZE:
            setMazeCommand.setFile(string);
            receiver.sendCommand(setMazeCommand);
            break;
        default:
            Logger.getInstance().send("AdvancedController", "Not handled input in Advanced Controller");
            break;
        }
    }

    @Override
    public void attachReceiver(CommandReceiver receiver) {
        this.receiver = receiver;
    }
}
