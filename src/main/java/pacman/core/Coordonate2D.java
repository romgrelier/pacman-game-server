package pacman.core;

public class Coordonate2D {
    public int x;
    public int y;

    public Coordonate2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    boolean equals(Coordonate2D c) {
        return c.x == x && c.y == y;
    }

    @Override
    public boolean equals(Object obj) {
        return this.equals((Coordonate2D) obj);
    }

    public boolean equals(int x, int y) {
        return this.x == x && this.y == y;
    }

    @Override
    public String toString() {
        return "[x : " + x + " | y : + " + y + " ]";
    }
}