package pacman.view;

import pacman.Logger;
import pacman.mvc.Controller;
import pacman.mvc.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class CommandView extends View implements KeyListener {

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
        // first player
        case KeyEvent.VK_Z:
            controller.sendInput(Controller.Input.Z);
            break;
        case KeyEvent.VK_Q:
            controller.sendInput(Controller.Input.Q);
            break;
        case KeyEvent.VK_S:
            controller.sendInput(Controller.Input.S);
            break;
        case KeyEvent.VK_D:
            controller.sendInput(Controller.Input.D);
            break;
        // second player
        case KeyEvent.VK_I:
            controller.sendInput(Controller.Input.I);
            break;
        case KeyEvent.VK_J:
            controller.sendInput(Controller.Input.J);
            break;
        case KeyEvent.VK_K:
            controller.sendInput(Controller.Input.K);
            break;
        case KeyEvent.VK_L:
            controller.sendInput(Controller.Input.L);
            break;
        // third player
        case KeyEvent.VK_UP:
            controller.sendInput(Controller.Input.I);
            break;
        case KeyEvent.VK_LEFT:
            controller.sendInput(Controller.Input.J);
            break;
        case KeyEvent.VK_DOWN:
            controller.sendInput(Controller.Input.K);
            break;
        case KeyEvent.VK_RIGHT:
            controller.sendInput(Controller.Input.L);
            break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    public enum Button {
        START, RESTART, PAUSE, STEP
    }

    private JPanel topPanel = new JPanel();
    private JPanel botPanel = new JPanel();

    private JButton restartButton;
    private JButton runButton;
    private JButton stepButton;
    private JButton pauseButton;

    private JSlider slider;
    private Label tourCount;
    private JButton fileChooser;

    public CommandView() {
        this.setSize(400, 200);
        this.setLayout(new GridLayout(2, 1));

        this.add(topPanel);
        this.add(botPanel);

        // Top Panel
        topPanel.setLayout(new GridLayout(1, 4));

        Icon icon_restart = new ImageIcon("res/icon/icon_restart.png");
        restartButton = new JButton(icon_restart);
        topPanel.add(restartButton);
        restartButton.addActionListener(e -> {
            controller.sendInput(Controller.Input.RESET);
        });

        Icon icon_run = new ImageIcon("res/icon/icon_run.png");
        runButton = new JButton(icon_run);
        topPanel.add(runButton);
        runButton.addActionListener(e -> {
            controller.sendInput(Controller.Input.RUN);
        });

        Icon icon_step = new ImageIcon("res/icon/icon_step.png");
        stepButton = new JButton(icon_step);
        topPanel.add(stepButton);
        stepButton.addActionListener(e -> {
            controller.sendInput(Controller.Input.STEP);
        });

        Icon icon_pause = new ImageIcon("res/icon/icon_pause.png");
        pauseButton = new JButton(icon_pause);
        topPanel.add(pauseButton);
        pauseButton.addActionListener(e -> {
            controller.sendInput(Controller.Input.PAUSE);
        });

        // Bot Panel
        botPanel.setLayout(new GridLayout(1, 3));

        slider = new JSlider(JSlider.HORIZONTAL, 1, 10, 1);
        botPanel.add(slider);
        slider.addChangeListener(e -> {
            controller.sendInput(Controller.Input.SETSPEED, slider.getValue());
        });
        /*
         * tourCount = new Label("Hello !"); botPanel.add(tourCount);
         */
        fileChooser = new JButton("choose a maze");
        botPanel.add(fileChooser);
        fileChooser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    System.out.println(selectedFile.getName());
                    controller.sendInput(Controller.Input.CHGMAZE, selectedFile.getAbsolutePath());
                }

            }
        });

        JComboBox list = new JComboBox();
        File directory = new File("res/layout");

        for (String file : directory.list()) {
            list.addItem(file);
        }

        list.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                controller.sendInput(Controller.Input.CHGMAZE, "res/layout/" + itemEvent.getItem());
            }
        });

        botPanel.add(list);

        this.addKeyListener(this);

        this.setVisible(true);
        this.requestFocus();
    }

    private void setButton(boolean state, Button button) {
        switch (button) {
        case PAUSE:
            pauseButton.setEnabled(state);
            break;
        case RESTART:
            restartButton.setEnabled(state);
            break;
        case START:
            runButton.setEnabled(state);
            break;
        case STEP:
            stepButton.setEnabled(state);
            break;
        }
    }

    @Override
    public void update(Event event) {
        switch (event) {
        case RESTART:
            setButton(false, Button.PAUSE);
            setButton(false, Button.RESTART);
            setButton(true, Button.START);
            setButton(true, Button.STEP);
            break;
        case RUN:
            setButton(true, Button.PAUSE);
            setButton(true, Button.RESTART);
            setButton(false, Button.START);
            setButton(false, Button.STEP);
            break;
        case STEP:
            setButton(false, Button.PAUSE);
            setButton(true, Button.RESTART);
            setButton(true, Button.START);
            setButton(true, Button.STEP);
            break;
        case PAUSE:
            setButton(false, Button.PAUSE);
            setButton(true, Button.RESTART);
            setButton(true, Button.START);
            setButton(true, Button.STEP);
            break;
        }

        this.requestFocus();
    }
}
