package pacman.view;

import pacman.command.Command;
import pacman.command.game.GetTourCount;
import pacman.mvc.View;

import javax.swing.*;

public class GameView extends View {
    private Command getTourCount = new GetTourCount(this);
    private JLabel tourCount = new JLabel("0");

    public GameView() {
        this.add(tourCount);
        this.setSize(600, 600);
        this.setVisible(true);
    }

    public void setTourCount(int tourCount, int maxTourCount) {
        this.tourCount.setText(tourCount + " / " + maxTourCount);
    }

    @Override
    public void update(Event event) {
        model.sendCommand(getTourCount);
    }
}
