package pacman.view;

import pacman.command.game.GetMaze;
import pacman.game.Maze;
import pacman.mvc.View;

public class MazeGameView extends View {
    private PanelPacmanGame mazePanel;
    private GetMaze getMazeCommand = new GetMaze();

    public MazeGameView() {
        this.setSize(600, 600);
        this.setVisible(true);
        getMazeCommand.setMazeGameView(this);
    }

    public void changeMaze(Maze maze) {
        mazePanel = new PanelPacmanGame(maze);
    }

    @Override
    public void update(Event event) {
        switch (event) {
        case UPDATE:
            if (mazePanel != null) {
                mazePanel.repaint();
            }
            break;
        case CHGMAZE:
            if (mazePanel != null) {
                this.remove(mazePanel);
            }
            model.sendCommand(getMazeCommand);
            mazePanel.setVisible(true);
            this.add(mazePanel);
            this.pack();
            this.setSize(600, 600);
            break;
        }
    }
}
