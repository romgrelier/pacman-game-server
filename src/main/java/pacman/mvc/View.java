package pacman.mvc;

import pacman.observer.Observer;

import javax.swing.*;

public abstract class View extends JFrame implements Observer {
    protected Controller controller;
    protected Model model;

    public void attachController(Controller controller) {
        this.controller = controller;
    }

    public void attachModel(Model model) {
        this.model = model;
        model.addObserver(this);
    }
}
