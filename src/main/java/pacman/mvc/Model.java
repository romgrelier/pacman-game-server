package pacman.mvc;

import pacman.command.Command;
import pacman.command.CommandReceiver;
import pacman.observer.Observer;
import pacman.observer.Subject;

import java.util.ArrayList;

public abstract class Model implements Subject, CommandReceiver, Runnable {
    private ArrayList<Observer> observers = new ArrayList<>();

    @Override
    public void sendCommand(Command command) {
        if (command.getInvoker() == null) {
            command.setInvoker(this);
        }
        command.execute();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver(Observer.Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }
}
