package pacman.mvc;

import pacman.command.CommandInvoker;
import pacman.command.CommandReceiver;

public abstract class Controller implements CommandInvoker {
    public enum Input {
        Z, Q, S, D, I, J, K, L, RESET, RUN, STEP, PAUSE, QUIT, SETSPEED, CHGMAZE, CNCT
    }

    protected CommandReceiver receiver;

    public abstract void sendInput(Input input);

    public abstract void sendInput(Input input, String string);

    public abstract void sendInput(Input input, long number);

}
