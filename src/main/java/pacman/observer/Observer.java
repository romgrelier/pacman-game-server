package pacman.observer;

public interface Observer {
    public enum Event {
        UPDATE, CHGMAZE, RESTART, RUN, STEP, PAUSE, END
    }

    void update(Event event);
}
