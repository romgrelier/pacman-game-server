package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;
import pacman.observer.Observer;

public class RunCommand extends Command<Game> {

    @Override
    public void execute() {
        invoker.start();
        invoker.notifyObserver(Observer.Event.RUN);
    }

    @Override
    public void cancel() {

    }
}
