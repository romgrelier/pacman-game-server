package pacman.command.game;

import pacman.command.Command;
import pacman.game.Direction;
import pacman.game.PacmanGame;
import pacman.game.player.Player;

public class InputMoveCommand extends Command<PacmanGame> {
    private Player player;
    private Direction direction;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.getDummy().setDirection(direction);
    }

    @Override
    public void cancel() {

    }
}
