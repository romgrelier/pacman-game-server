package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;
import pacman.observer.Observer;

public class QuitCommand extends Command<Game> {

    @Override
    public void execute() {
        invoker.stop();
        invoker.notifyObserver(Observer.Event.PAUSE);
    }

    @Override
    public void cancel() {

    }
}
