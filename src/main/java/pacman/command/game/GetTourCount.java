package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;
import pacman.view.GameView;

public class GetTourCount extends Command<Game> {
    private GameView gameView;

    public GetTourCount(GameView gameView) {
        this.gameView = gameView;
    }

    @Override
    public void execute() {
        gameView.setTourCount(invoker.getTourCount(), invoker.getMaxTourCount());
    }

    @Override
    public void cancel() {

    }
}
