package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;

public class SetSpeed extends Command<Game> {
    private long speed;

    public void setSpeed(long speed) {
        this.speed = speed;
    }

    @Override
    public void execute() {
        invoker.setSpeed(speed);
    }

    @Override
    public void cancel() {

    }
}
