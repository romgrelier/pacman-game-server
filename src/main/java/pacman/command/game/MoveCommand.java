package pacman.command.game;

import pacman.command.Command;
import pacman.game.Direction;
import pacman.game.Maze;
import pacman.game.Position;
import pacman.game.actor.Actor;

public class MoveCommand extends Command<Actor> {
    private Direction direction;
    private Maze maze;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setMaze(Maze maze) {
        this.maze = maze;
    }

    @Override
    public void execute() {
        Position position = new Position(invoker.getPosition());
        position.setDirection(this.direction);
        position.moveForward();

        if (isLegalMove(position)) {
            invoker.move(direction);
        }
    }

    @Override
    public void cancel() {

    }

    private boolean isLegalMove(Position newPosition) {
        return newPosition.getX() < maze.getSizeX() && newPosition.getX() >= 0 && newPosition.getY() < maze.getSizeY()
                && newPosition.getX() >= 0 && !maze.isWall(newPosition.getX(), newPosition.getY());
    }
}
