package pacman.command.game;

import pacman.command.Command;
import pacman.game.PacmanGame;

public class SetMaze extends Command<PacmanGame> {
    String file;

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public void execute() {
        try {
            invoker.changeMaze(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancel() {

    }
}
