package pacman.command.game;

import pacman.command.Command;
import pacman.controller.AdvancedController;
import pacman.game.PacmanGame;

public class RequestPlayerCreate extends Command<PacmanGame> {
    private AdvancedController controller;
    private int id = 0;

    public void setController(AdvancedController controller) {
        this.controller = controller;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        controller.setPlayer(id, invoker.addPlayer());
    }

    @Override
    public void cancel() {

    }
}
