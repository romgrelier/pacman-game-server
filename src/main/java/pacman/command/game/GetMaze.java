package pacman.command.game;

import pacman.command.Command;
import pacman.game.PacmanGame;
import pacman.view.MazeGameView;

public class GetMaze extends Command<PacmanGame> {
    private MazeGameView mazeGameView;

    public void setMazeGameView(MazeGameView mazeGameView) {
        this.mazeGameView = mazeGameView;
    }

    @Override
    public void execute() {
        mazeGameView.changeMaze(invoker.getMaze());
    }

    @Override
    public void cancel() {

    }
}
