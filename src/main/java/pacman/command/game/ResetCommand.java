package pacman.command.game;

import pacman.command.Command;
import pacman.game.Game;
import pacman.observer.Observer;

public class ResetCommand extends Command<Game> {

    @Override
    public void execute() {
        invoker.stop();
        invoker.init();
        invoker.notifyObserver(Observer.Event.RESTART);
    }

    @Override
    public void cancel() {

    }
}
