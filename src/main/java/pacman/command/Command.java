package pacman.command;

public abstract class Command<T> {
    protected T invoker;

    public Command(T invoker) {
        this.invoker = invoker;
    }

    public Command() {

    }

    public void setInvoker(T invoker) {
        this.invoker = invoker;
    }

    public T getInvoker() {
        return invoker;
    }

    public abstract void execute();

    public abstract void cancel();
}
